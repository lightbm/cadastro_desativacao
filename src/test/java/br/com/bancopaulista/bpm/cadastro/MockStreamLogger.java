/**
 * 
 */
package br.com.bancopaulista.bpm.cadastro;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author LightHouse
 * Implementa��o simpl�ria de logger para subprocessos.
 *
 */
public class MockStreamLogger {
	
	private static final Log log = LogFactory.getLog(MockStreamLogger.class);
	
	
	public MockStreamLogger() {
		
	}

	
	public void addStream(InputStream stream) {
		
		Runnable r =  new StreamReader(stream);
		Thread t = new Thread(r);
		t.start();
	}
	
	
	private class StreamReader implements Runnable {
		
		private final InputStream stream;
		
		StreamReader(InputStream stream) {
			this.stream = stream;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
			int n = -1;
			byte[] buffer = new byte[4096];
			
			while (true) {

				try {
					n = -1;
					n = stream.read(buffer);
				}
				catch(IOException ignored){}
				
				if ( n < 0) {
					// EOF ou erro, n�o me importa
					return;
				}
			}
		}
	}
}
