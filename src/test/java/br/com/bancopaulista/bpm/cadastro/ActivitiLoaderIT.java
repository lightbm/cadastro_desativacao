package br.com.bancopaulista.bpm.cadastro;

import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipInputStream;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;

public class ActivitiLoaderIT {
	
	public static void main(String[] args) throws Exception {
		
		ProcessEngine pe = ProcessEngines.getDefaultProcessEngine();
		
		args = new String[] {"target/cadastro-bloqueio-1.0-SNAPSHOT-asgard-bpm-package.zip"};
		
		for (String pack : args) {
			File f = new File(pack);
			
			if ( !f.isFile() || !f.canRead()) {
				System.err.println("[E19] Arquivo invalido: '" + f + "'");
				continue;
			}
			
			
			ZipInputStream zip = new ZipInputStream(new FileInputStream(f));
			
			DeploymentBuilder db = pe.getRepositoryService().createDeployment();
			Deployment deploy = db
				.addZipInputStream(zip)
				.name(f.getName()).deploy();
			
			System.out.println("[I34] Deploy ok: file=" + f.getName() + ", id=" + deploy.getId());
			
		}
		
		
	}

}
