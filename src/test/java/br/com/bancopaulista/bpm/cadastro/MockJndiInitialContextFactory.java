/**
 * 
 */
package br.com.bancopaulista.bpm.cadastro;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

/**
 * @author Philippe
 *
 */
public class MockJndiInitialContextFactory implements InitialContextFactory {
	
	private static Context currentContext = new MockJndiContext();
	

	/* (non-Javadoc)
	 * @see javax.naming.spi.InitialContextFactory\#getInitialContext(java.util.Hashtable)
	 */
	@Override
	public Context getInitialContext(Hashtable<?, ?> environment)
			throws NamingException {
		
		
		return currentContext;
	}

}
