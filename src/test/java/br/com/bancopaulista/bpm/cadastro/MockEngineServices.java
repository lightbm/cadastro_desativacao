/**
 * 
 */
package br.com.bancopaulista.bpm.cadastro;

import org.activiti.engine.EngineServices;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;

/**
 * @author LightHouse
 *
 */
public class MockEngineServices implements EngineServices {

	/* (non-Javadoc)
	 * @see org.activiti.engine.EngineServices#getRepositoryService()
	 */
	@Override
	public RepositoryService getRepositoryService() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.EngineServices#getRuntimeService()
	 */
	@Override
	public RuntimeService getRuntimeService() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.EngineServices#getFormService()
	 */
	@Override
	public FormService getFormService() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.EngineServices#getTaskService()
	 */
	@Override
	public TaskService getTaskService() {
		// TODO Auto-generated method stub
		return new MockTaskService();
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.EngineServices#getHistoryService()
	 */
	@Override
	public HistoryService getHistoryService() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.EngineServices#getIdentityService()
	 */
	@Override
	public IdentityService getIdentityService() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.EngineServices#getManagementService()
	 */
	@Override
	public ManagementService getManagementService() {
		// TODO Auto-generated method stub
		return null;
	}

}
