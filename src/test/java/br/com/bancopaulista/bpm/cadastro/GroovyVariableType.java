package br.com.bancopaulista.bpm.cadastro;


import groovy.json.JsonOutput;
import groovy.json.JsonParserType;
import groovy.json.JsonSlurper;

import java.util.Collection;
import java.util.Map;

import org.activiti.engine.impl.variable.ValueFields;
import org.activiti.engine.impl.variable.VariableType;

/**
 * Logica para serializacio de variaveis JSON na base de dados
 * @author Philippe
 *
 */
public class GroovyVariableType implements VariableType {

	private JsonSlurper slurper;
	
	public GroovyVariableType() {
		slurper = new JsonSlurper();
		slurper.setType(JsonParserType.LAX); // Uso o LAX para ser bem camarada
		slurper.setCheckDates(true);
		
	}

	public String getTypeName() {
		return "jsonString";
	}

	@Override
	public boolean isCachable() {
		return true;
	}

	@Override
	public boolean isAbleToStore(Object value) {
		if ( value instanceof Map<?, ?> || value instanceof Object[] || value instanceof Collection<?>) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void setValue(Object value, ValueFields valueFields) {

		valueFields.setCachedValue(value);
		if ( value == null) {
			valueFields.setTextValue2(null);
		}
		else {
			String json = JsonOutput.toJson(value);
			valueFields.setTextValue2(json);
		}
	}

	@Override
	public Object getValue(ValueFields valueFields) {
		Object o = valueFields.getCachedValue();
		if ( o != null) {
			return o;
		}
		
		String json = valueFields.getTextValue2();
		if ( json == null) {
			return null;
		}
		else {
			o = slurper.parseText(json);
			return o;
		}
	}

}
