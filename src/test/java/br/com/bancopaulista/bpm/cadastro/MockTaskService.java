/**
 * 
 */
package br.com.bancopaulista.bpm.cadastro;

import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Attachment;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Event;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.NativeTaskQuery;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author LightHouse
 *
 */
public class MockTaskService implements TaskService {
	
	private static final Log log = LogFactory.getLog(MockTaskService.class);

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#newTask()
	 */
	@Override
	public Task newTask() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#newTask(java.lang.String)
	 */
	@Override
	public Task newTask(String taskId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#saveTask(org.activiti.engine.task.Task)
	 */
	@Override
	public void saveTask(Task task) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteTask(java.lang.String)
	 */
	@Override
	public void deleteTask(String taskId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteTasks(java.util.Collection)
	 */
	@Override
	public void deleteTasks(Collection<String> taskIds) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteTask(java.lang.String, boolean)
	 */
	@Override
	public void deleteTask(String taskId, boolean cascade) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteTasks(java.util.Collection, boolean)
	 */
	@Override
	public void deleteTasks(Collection<String> taskIds, boolean cascade) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteTask(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteTask(String taskId, String deleteReason) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteTasks(java.util.Collection, java.lang.String)
	 */
	@Override
	public void deleteTasks(Collection<String> taskIds, String deleteReason) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#claim(java.lang.String, java.lang.String)
	 */
	@Override
	public void claim(String taskId, String userId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#complete(java.lang.String)
	 */
	@Override
	public void complete(String taskId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#delegateTask(java.lang.String, java.lang.String)
	 */
	@Override
	public void delegateTask(String taskId, String userId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#resolveTask(java.lang.String)
	 */
	@Override
	public void resolveTask(String taskId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#complete(java.lang.String, java.util.Map)
	 */
	@Override
	public void complete(String taskId, Map<String, Object> variables) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setAssignee(java.lang.String, java.lang.String)
	 */
	@Override
	public void setAssignee(String taskId, String userId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setOwner(java.lang.String, java.lang.String)
	 */
	@Override
	public void setOwner(String taskId, String userId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getIdentityLinksForTask(java.lang.String)
	 */
	@Override
	public List<IdentityLink> getIdentityLinksForTask(String taskId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#addCandidateUser(java.lang.String, java.lang.String)
	 */
	@Override
	public void addCandidateUser(String taskId, String userId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#addCandidateGroup(java.lang.String, java.lang.String)
	 */
	@Override
	public void addCandidateGroup(String taskId, String groupId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#addUserIdentityLink(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void addUserIdentityLink(String taskId, String userId,
			String identityLinkType) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#addGroupIdentityLink(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void addGroupIdentityLink(String taskId, String groupId,
			String identityLinkType) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteCandidateUser(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteCandidateUser(String taskId, String userId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteCandidateGroup(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteCandidateGroup(String taskId, String groupId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteUserIdentityLink(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteUserIdentityLink(String taskId, String userId,
			String identityLinkType) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteGroupIdentityLink(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteGroupIdentityLink(String taskId, String groupId,
			String identityLinkType) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setPriority(java.lang.String, int)
	 */
	@Override
	public void setPriority(String taskId, int priority) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setDueDate(java.lang.String, java.util.Date)
	 */
	@Override
	public void setDueDate(String taskId, Date dueDate) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#createTaskQuery()
	 */
	@Override
	public TaskQuery createTaskQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#createNativeTaskQuery()
	 */
	@Override
	public NativeTaskQuery createNativeTaskQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setVariable(java.lang.String, java.lang.String, java.lang.Object)
	 */
	@Override
	public void setVariable(String taskId, String variableName, Object value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setVariables(java.lang.String, java.util.Map)
	 */
	@Override
	public void setVariables(String taskId,
			Map<String, ? extends Object> variables) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setVariableLocal(java.lang.String, java.lang.String, java.lang.Object)
	 */
	@Override
	public void setVariableLocal(String taskId, String variableName,
			Object value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#setVariablesLocal(java.lang.String, java.util.Map)
	 */
	@Override
	public void setVariablesLocal(String taskId,
			Map<String, ? extends Object> variables) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getVariable(java.lang.String, java.lang.String)
	 */
	@Override
	public Object getVariable(String taskId, String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getVariableLocal(java.lang.String, java.lang.String)
	 */
	@Override
	public Object getVariableLocal(String taskId, String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getVariables(java.lang.String)
	 */
	@Override
	public Map<String, Object> getVariables(String taskId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getVariablesLocal(java.lang.String)
	 */
	@Override
	public Map<String, Object> getVariablesLocal(String taskId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getVariables(java.lang.String, java.util.Collection)
	 */
	@Override
	public Map<String, Object> getVariables(String taskId,
			Collection<String> variableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getVariablesLocal(java.lang.String, java.util.Collection)
	 */
	@Override
	public Map<String, Object> getVariablesLocal(String taskId,
			Collection<String> variableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#removeVariable(java.lang.String, java.lang.String)
	 */
	@Override
	public void removeVariable(String taskId, String variableName) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#removeVariableLocal(java.lang.String, java.lang.String)
	 */
	@Override
	public void removeVariableLocal(String taskId, String variableName) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#removeVariables(java.lang.String, java.util.Collection)
	 */
	@Override
	public void removeVariables(String taskId, Collection<String> variableNames) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#removeVariablesLocal(java.lang.String, java.util.Collection)
	 */
	@Override
	public void removeVariablesLocal(String taskId,
			Collection<String> variableNames) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#addComment(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void addComment(String taskId, String processInstanceId,	String message) {
		log.info(String.format("addComment: [%s,%s] - %s",taskId,processInstanceId,message));
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteComments(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteComments(String taskId, String processInstanceId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getTaskComments(java.lang.String)
	 */
	@Override
	public List<Comment> getTaskComments(String taskId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getTaskEvents(java.lang.String)
	 */
	@Override
	public List<Event> getTaskEvents(String taskId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getProcessInstanceComments(java.lang.String)
	 */
	@Override
	public List<Comment> getProcessInstanceComments(String processInstanceId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#createAttachment(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.io.InputStream)
	 */
	@Override
	public Attachment createAttachment(String attachmentType, String taskId,
			String processInstanceId, String attachmentName,
			String attachmentDescription, InputStream content) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#createAttachment(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Attachment createAttachment(String attachmentType, String taskId,
			String processInstanceId, String attachmentName,
			String attachmentDescription, String url) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#saveAttachment(org.activiti.engine.task.Attachment)
	 */
	@Override
	public void saveAttachment(Attachment attachment) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getAttachment(java.lang.String)
	 */
	@Override
	public Attachment getAttachment(String attachmentId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getAttachmentContent(java.lang.String)
	 */
	@Override
	public InputStream getAttachmentContent(String attachmentId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getTaskAttachments(java.lang.String)
	 */
	@Override
	public List<Attachment> getTaskAttachments(String taskId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getProcessInstanceAttachments(java.lang.String)
	 */
	@Override
	public List<Attachment> getProcessInstanceAttachments(
			String processInstanceId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#deleteAttachment(java.lang.String)
	 */
	@Override
	public void deleteAttachment(String attachmentId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.TaskService#getSubTasks(java.lang.String)
	 */
	@Override
	public List<Task> getSubTasks(String parentTaskId) {
		// TODO Auto-generated method stub
		return null;
	}

}
