/**
 * 
 */
package br.com.bancopaulista.bpm.cadastro;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.EngineServices;
import org.activiti.engine.delegate.DelegateExecution;

/**
 * @author LightHouse
 *
 */
public class MockDelegateExecution implements DelegateExecution {
	
	Map<String,Object> variables = new HashMap<String, Object>();
	private String pid;
	
	public MockDelegateExecution() {
		
	}

	public MockDelegateExecution(String pid) {
		this.pid = pid;
	}
	
	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#getVariables()
	 */
	@Override
	public Map<String, Object> getVariables() {
		return variables;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#getVariablesLocal()
	 */
	@Override
	public Map<String, Object> getVariablesLocal() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#getVariable(java.lang.String)
	 */
	@Override
	public Object getVariable(String variableName) {
		return variables.get(variableName);
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#getVariableLocal(java.lang.Object)
	 */
	@Override
	public Object getVariableLocal(Object variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#getVariableNames()
	 */
	@Override
	public Set<String> getVariableNames() {
		return variables.keySet();
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#getVariableNamesLocal()
	 */
	@Override
	public Set<String> getVariableNamesLocal() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#setVariable(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setVariable(String variableName, Object value) {
		variables.put(variableName,value);
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#setVariableLocal(java.lang.String, java.lang.Object)
	 */
	@Override
	public Object setVariableLocal(String variableName, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#setVariables(java.util.Map)
	 */
	@Override
	public void setVariables(Map<String, ? extends Object> variables) {

		for( Map.Entry<String, ?> entry : variables.entrySet()) {
			this.variables.put(entry.getKey(), entry.getValue());
		}
		
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#setVariablesLocal(java.util.Map)
	 */
	@Override
	public void setVariablesLocal(Map<String, ? extends Object> variables) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#hasVariables()
	 */
	@Override
	public boolean hasVariables() {
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#hasVariablesLocal()
	 */
	@Override
	public boolean hasVariablesLocal() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#hasVariable(java.lang.String)
	 */
	@Override
	public boolean hasVariable(String variableName) {		
		Object o = this.variables.get(variableName);
		return o != null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#hasVariableLocal(java.lang.String)
	 */
	@Override
	public boolean hasVariableLocal(String variableName) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#createVariableLocal(java.lang.String, java.lang.Object)
	 */
	@Override
	public void createVariableLocal(String variableName, Object value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#removeVariable(java.lang.String)
	 */
	@Override
	public void removeVariable(String variableName) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#removeVariableLocal(java.lang.String)
	 */
	@Override
	public void removeVariableLocal(String variableName) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#removeVariables(java.util.Collection)
	 */
	@Override
	public void removeVariables(Collection<String> variableNames) {
		for( String name : variableNames) {
			variables.remove(name);
		}
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#removeVariablesLocal(java.util.Collection)
	 */
	@Override
	public void removeVariablesLocal(Collection<String> variableNames) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#removeVariables()
	 */
	@Override
	public void removeVariables() {
		variables.clear();
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.VariableScope#removeVariablesLocal()
	 */
	@Override
	public void removeVariablesLocal() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getId()
	 */
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getProcessInstanceId()
	 */
	@Override
	public String getProcessInstanceId() {
		// TODO Auto-generated method stub
		return pid;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getEventName()
	 */
	@Override
	public String getEventName() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getBusinessKey()
	 */
	@Override
	public String getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getProcessBusinessKey()
	 */
	@Override
	public String getProcessBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getProcessDefinitionId()
	 */
	@Override
	public String getProcessDefinitionId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getParentId()
	 */
	@Override
	public String getParentId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getCurrentActivityId()
	 */
	@Override
	public String getCurrentActivityId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getCurrentActivityName()
	 */
	@Override
	public String getCurrentActivityName() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.DelegateExecution#getEngineServices()
	 */
	@Override
	public EngineServices getEngineServices() {
		// TODO Auto-generated method stub
		return new MockEngineServices();
	}

}
