
package br.com.bancopaulista.bpm.cadastro;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.context.ApplicationContext;

public class AppContextScriptAdapter implements Map<String,Object>{

	private ApplicationContext context;
	public AppContextScriptAdapter(ApplicationContext context) {
		this.context = context;
		
	}
	@Override
	public int size() {
		return context.getBeanDefinitionCount();
	}

	@Override
	public boolean isEmpty() {
		return context.getBeanDefinitionCount() == 0;
	}

	@Override
	public boolean containsKey(Object key) {
		if ( key == null ) {
			return false;
		}
		return context.containsBean(key.toString());
	}

	@Override
	public boolean containsValue(Object value) {
		return false;
	}

	@Override
	public Object get(Object key) {
		if ( key == null ) {
			return null;
		}
		
		Object o =  context.getBean(key.toString());
		return o;
	}

	@Override
	public Object put(String key, Object value) {
		throw new UnsupportedOperationException("put is not allowed");
	}

	@Override
	public Object remove(Object key) {
		throw new UnsupportedOperationException("remove is not allowed");
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		throw new UnsupportedOperationException("putAll is not allowed");
		
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("clear is not allowed");
		
	}

	@Override
	public Set<String> keySet() {
		HashSet<String> keys = new HashSet<String>();
		keys.addAll(Arrays.asList(context.getBeanDefinitionNames()));
		
		return keys;
	}

	@Override
	public Collection<Object> values() {
		return context.getBeansOfType(Object.class).values();
	}

	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		return  context.getBeansOfType(Object.class).entrySet();
	}

}
