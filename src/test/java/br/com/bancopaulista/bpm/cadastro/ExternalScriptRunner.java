package br.com.bancopaulista.bpm.cadastro;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleScriptContext;

import org.activiti.engine.EngineServices;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;


/**
 * Task para executar scripts no contexto de um task do activiti, permitindo assim que estes sejam
 * externalizados
 * @author LightHouse
 *
 */
public class ExternalScriptRunner implements ApplicationContextAware {
	
	private static final Log log = LogFactory.getLog(ExternalScriptRunner.class);
	
	
	private ApplicationContext appContext;
	
	private String scriptRoot = "";
	
	
	private String systemErrorCode = "SystemError"; 
	
	/**
	 * @return the systemErrorCode
	 */
	public String getSystemErrorCode() {
		return systemErrorCode;
	}


	/**
	 * @param systemErrorCode the systemErrorCode to set
	 */
	public void setSystemErrorCode(String systemErrorCode) {
		this.systemErrorCode = systemErrorCode;
	}


	/**
	 * @return the scriptRoot
	 */
	public String getScriptRoot() {
		return scriptRoot;
	}


	/**
	 * @param scriptRoot the scriptRoot to set
	 */
	public void setScriptRoot(String scriptRoot) {
		this.scriptRoot = scriptRoot;
	}


	public ExternalScriptRunner() {
		
	}


	/**
	 * 
	 * @param script Caminho para recurso contendo o script
	 * @param execution Objeto passado no contexto de execu��o do script. Normalmente deve ser o 
	 *                  "execution" do activiti, mas isto n�o � obrigat�rio
	 * @return
	 */
	public Object execute(String script, Object execution, String engineName)  {
		
		// Tratamentos especial para o Groovy
		if ( "groovy".equalsIgnoreCase(engineName) ) {
			return executeGroovyScript(script, execution);
		}
		
		Resource scriptResource = appContext.getResource(script);
		if ( scriptResource == null || !scriptResource.exists() ) {
			throw new IllegalArgumentException("Invalid script resource: " + script);
		}
		
		
		// Ajuda para depura��o
		if ( log.isInfoEnabled()) {
			log.info("[I55] script='" + script + "', engineName=" + engineName);
			if ( execution instanceof DelegateExecution) {
				DelegateExecution deg = (DelegateExecution)execution;
				StringBuilder b = new StringBuilder();
				b.append("execution: processBussinessKey=").append(deg.getProcessBusinessKey())
				.append(", currentActivityName=").append(deg.getCurrentActivityName())
				.append(", processInstanceId=").append(deg.getProcessInstanceId())
				.append(", processDefinitionId=").append(deg.getProcessDefinitionId());
				
				log.info(b.toString());
			}
			else {
				log.info("execution=" + execution);
			}
		}
		
		// Executa o script
		ScriptEngineManager m = new ScriptEngineManager();
		ScriptEngine engine = m.getEngineByName(engineName);
		ScriptContext ctx = new SimpleScriptContext();
		
		Bindings binds = ctx.getBindings(ScriptContext.ENGINE_SCOPE);
		binds.put("log", log);
		binds.put("execution", execution);
		binds.put("appContext", new AppContextScriptAdapter(appContext));
		
		try {
			InputStream in = scriptResource.getInputStream();
			try {
				if ( in != null ) {
					Object result = engine.eval(new InputStreamReader(in), ctx);
					return result;
				}
			
				throw new IllegalArgumentException("Recurso n�o encontrado: " + script);
			}
			finally {
				if ( in != null ) {
					try {
						in.close();
					}
					catch(Exception ignored){}
				}
			}
		}
		catch(Exception ex) {
			log.error("Erro executando script. script='" + script + "', execution=" + execution, ex);
			throw new RuntimeException(ex);
		}
		
	}


	/**
	 * Vers�o especializada para o caso em que o script est� em JS
	 * @param script
	 * @param execution
	 * @return
	 */
	public Object executeJavaScript(String script, Object execution) {
		
		return execute(script,execution,"JavaScript");
	}


	/**
	 * Vers�o especializada para o caso em que o script est� em Groovy
	 * @param script
	 * @param execution
	 * @return
	 */
	public Object executeGroovyScript(String script, Object execution) {

		ClassLoader parent = Thread.currentThread().getContextClassLoader();
		GroovyClassLoader loader = new GroovyClassLoader(parent);
		try {
			
			if ( !script.endsWith(".groovy")) {
				// Acrescenta extens�o padr�o
				script = script  + ".groovy";
			}
			
			log.info("[I197] executeGroovyScript: script='" + script + "'");
			long start = System.currentTimeMillis();
			
			InputStream in = getBPMResource((DelegateExecution)execution,script);
			if ( in == null) {
				throw new IllegalArgumentException("Script n�o encontrado: " + script);
			}
			
			GroovyCodeSource source = new GroovyCodeSource(new InputStreamReader(in), script, script);
			Class groovyClass = loader.parseClass(source,true);

			// let's call some method on an instance
			GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
			Object[] args = {execution, new AppContextScriptAdapter(appContext)};
			
			log.info("[I212] script criado. elapsed=" + (System.currentTimeMillis()-start));
			try {
				return groovyObject.invokeMethod("execute", args);
			}
			finally {
				log.info("[I217] script executado. elapsed=" + (System.currentTimeMillis()-start));				
			}
		}
		catch(Exception ex) {
			log.error("[E177] Erro executando script '" + script + "': "  + ex.getMessage(),ex);
			throw new RuntimeException(ex.getMessage(),ex);
		}
	}
	
	
	/**
	 * Recupera um script, que pode estar definido no ambiente ou diretamente dentro do pacote do processo em execu��o.
	 * A prefer�ncia � sempre para o recurso externalizado
	 * @param execution
	 * @param script
	 * @return
	 */
	protected InputStream getBPMResource(DelegateExecution execution, String script) {
		
		try {
			EngineServices processEngine = execution.getEngineServices();
			
			List<ProcessDefinition> defs = processEngine.getRepositoryService().createProcessDefinitionQuery().processDefinitionId(execution.getProcessDefinitionId()).list();
			ProcessDefinition def = defs.get(0);

			// Substitui '.'s no nome por '/'
			
			String processPath = def.getKey().replaceAll("\\.", "/");
			
			// Temos um recurso com o mesmo nome na pasta /processo/vers�o/ ?
			String externalName = getScriptRoot() + "/" +  processPath + "/" + def.getVersion() + "/" + script;
			Resource r = appContext.getResource(externalName);
			
			if ( !r.exists()) {
				// Tenta mais uma vez, sem a vers�o
				externalName = getScriptRoot() + "/" +  processPath + "/" + script;
				r = appContext.getResource(externalName);
			}
			
			InputStream in = null;
			if ( r.exists()) {
				log.debug("getBPMResource: script='" + script + "' => resource=" + r.getURI());
				in = r.getInputStream();
			}
			else {
				String deploymentId = def.getDeploymentId();
				log.debug("processDefinitionId=" + execution.getProcessDefinitionId() + " => deploymentId=" + deploymentId);
				in = processEngine.getRepositoryService().getResourceAsStream(deploymentId, "/" + processPath +  "/" + script);
			}
			
			return in;
		}
		catch(Throwable t ) {
			log.error("[E235] Recurso BPM n�o localizado: '" + script + "'",t);
			return null;
		}
	}


	@Override
	public void setApplicationContext(ApplicationContext appContext)
			throws BeansException {
		this.appContext = appContext;
	}



}
