<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#formPessoa" data-toggle="tab">Formul�rio</a>
		</li>
		<li><a href="#errorList" data-toggle="tab">Erros</a>
		</li>
		<li><a href="#comentarios" data-toggle="tab">Coment�rios</a>
		</li>
		<li><a href="#instrucoes" data-toggle="tab">Instru��es</a>
		</li>
	</ul>
	<div class="tab-content" id="frmRevisarErrosDesativacao" data-bind="with: data">
		<div class="tab-pane active" id="formPessoa">

			<div class="row-fluid">										
				<div class="span3">
					Id cliente Matera <br/> 
					<input	type="text" name="idPessoaMatera" class="input-block-level" data-bind="value:idPessoaMatera" readonly="readonly"/>
				</div>
				<div class="span3">
					Inscri��o (CPF ou CNPJ) <br/> 
					<input	type="text" name="inscricao" class="input-block-level" data-bind="value:inscricao" readonly="readonly"/>
				</div>
				
			</div>

			<div class="row-fluid">										
				<div class="span6">
					Nome/Raz�o social<br/> 
					<input	type="text" name="nomeCliente" class="input-block-level" data-bind="value:nomeCliente" readonly="readonly"/>
				</div>
			</div>
			
		</div>

<!-- ***************************************************************************   LISTA DE ERROS: BEGIN -->
		<div class="tab-pane" id="errorList" >
			<div class="row-fluid">
			<div class="span12">
				<table class="table table-condensed table-bordered">
					<thead>
						<tr>
							<th>Tipo</th>
							<th>Descri��o</th>
							<th>Mensagem</th>
						</tr>				
					</thead>
					<tbody data-bind="foreach:statusAtivacao">
						<tr data-bind="css: { info : success , error: !success}">
							<td>
								<span data-bind="text: success?'INFO':'ERRO'">&nbsp;&nbsp;&nbsp;&nbsp;</span>
							</td>
							<td data-bind="text: desc"></td>
							<td data-bind="text: msg"></td>
						</tr>
					</tbody>
				</table>
			</div>
			</div>
		</div>		
<!-- ***************************************************************************   LISTA DE ERROS: END -->
	
		
	<div class="tab-pane" id="comentarios">
		<div class="row-fluid">
			<div class="span12">
				<table class="table table-condensed table-bordered">
					<thead>
						<tr>
							<th>Data</th>
							<th>Usu�rio</th>
							<th>Coment�rio</th>
						</tr>				
					</thead>
					<tbody data-bind="foreach:comentarios">
						<tr>
							<td data-bind="text: formatDateTime(time)"></td>
							<td data-bind="text:userId"></td>
							<td data-bind="text: fullMessage"></td>
						</tr>
					</tbody>
				</table>			
			</div>
		</div>
				
	</div>
	
	<div class="tab-pane" id="instrucoes">
		<div class="row-fluid">
			<div class="span12">
			<pre>
				${task.description}			
			</pre>
			</div>
		</div>
	</div>
	
	</div>
</div>

<%@ include file="frmRevisarErrosDesativacao_js.jspf" %>


