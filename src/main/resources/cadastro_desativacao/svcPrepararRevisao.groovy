package cadastro_desativacao

import java.util.Map;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;

import cadastro_common.AtivacaoHelper;
import groovy.json.JsonOutput;
import groovy.json.JsonSlurper;
import groovyx.net.http.HTTPBuilder;
import groovyx.net.http.Method;



/**
 * Entry point chamado pelo framrwork
 * @param execution objeto do tipo DelegateExecution do Activiti
 * @param appContext ApplicationContext do Sprint
 */
public class svcPrepararRevisao {
	def execute( DelegateExecution execution, Map<String, Object> appContext ) {


		def statusAtivacao = execution.getVariable("statusAtivacao")
		
		// Define o usu�rio para receber a tarefa de revis�o. O usu�rio preferencial � aquele que realizou
		// o cadastro.
		def ativCadastro ="userVerificarErrosSolicitacao"; 

		List<HistoricTaskInstance> tasks = execution.engineServices.historyService.createHistoricTaskInstanceQuery()
				.processInstanceId(execution.processInstanceId)
				.taskDefinitionKey(ativCadastro)
				.orderByHistoricTaskInstanceStartTime().desc().list();

		if ( tasks == null || tasks.isEmpty()) {
			execution.setVariable("revisor", null);
		}
		else {
			def revisor = tasks.get(0).assignee;
			log.info("[D045] revisor=${revisor}");
			execution.setVariable("revisor", revisor);
		}

	}


	def saveMsgAtivacao(DelegateExecution execution, desc, success, msg ) {
		AtivacaoHelper.saveMsgAtivacao(execution, desc, success, msg);
	}

}
