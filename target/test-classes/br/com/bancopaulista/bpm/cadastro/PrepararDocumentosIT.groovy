package br.com.bancopaulista.bpm.cadastro

import cadastro_bloqueio.svcPrepararDocumentos
import org.activiti.engine.delegate.DelegateExecution
import org.junit.Test;

class PrepararDocumentosIT {
	@Test
	public void execute() {
		def appContext = [
			"dataServicesEndpoint" : "http://172.16.14.43:8080/esbproxy/services"
		]
		def exec = new MockDelegateExecution("19999")
		exec.setVariable("inscricao", "19980856000107")
		exec.setVariable("dataReferencia", "2018-12-05")
		def teste = new svcPrepararDocumentos()	
		teste.execute(exec,appContext)	
	}
}
