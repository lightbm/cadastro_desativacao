package br.com.bancopaulista.bpm.cadastro;

import java.util.Map;

import cadastro_bloqueio.svcBloqueioCliente
import cadastro_bloqueio.services.OperacaoBPMService
import cadastro_common.MateraServices

import org.apache.commons.collections.map.HashedMap
import org.junit.Test;

class svcBloqueioClienteIT {
	@Test
	public void teste() {
		def bloqueio = new svcBloqueioCliente()
		def execution = new MockDelegateExecution()
		execution.setVariable("inscricao", "44665881715")
		execution.setVariable("dataReferencia", "2018-03-31")
		def cliente = new OperacaoBPMService("http://172.16.14.43:8280/services/bpm_operacao").obterFichaCliente(44665881715)
		//def cliente = '{"dadosBackend":{},"dadosConjuge":{"cpf":"","documento":"","nome":""},"dadosPatrimonio":{"outrosRendimentos":500,"patrimonio":{"bensImoveis":150000,"bensMoveis":8000,"bensOutros":""},"rendimentoMensal":1200},"dadosPessoais":{"celular":{"ddd":"","ddi":"","numero":""},"clienteSistemaFinanceiroDesde":null,"dataNascimento":"01/01/1970","docAdicional":{"dataEmissao":null,"emissor":{"nome":"","pais":"","uf":""},"numero":"","tipo":""},"domiciliadoExterior":"N","email":"cvl@gmail.com","endereco":{"bairro":"JARDIM PAULISTA","cep":"01403-010","complemento":"","logradouro":"AL SARUTAIA","municipio":"SAO PAULO","numero":"100","pais":"BRASIL","uf":"SP"},"enderecoCorrespondencia":{"bairro":"JARDIM PAULISTA","cep":"01407-000","complemento":"","logradouro":"","municipio":"SAO PAULO","numero":"900","pais":"BRASIL","uf":"SP"},"estadoCivil":"1","extratoPapel":"N","idPessoa":"","inscricao":"991.237.562-92","localCorrespondencia":"R","localNascimento":"FORTALEZA","nacionalidade":"BRASILEIRA","nome":"CARLOS LAVA JATO","nomeAbreviado":null,"nomeMae":"MARIA LAVA JATO","nomePai":"","paisNascimento":"BRASIL","rg":{"emissao":"01/01/1980","emissor":{"nome":"SSP","uf":"CE"},"numero":"44222333"},"sexo":"M","telefone":{"ddd":"11","ddi":"","numero":"33332222"},"tipoPessoa":"F","ufNascimento":"CE"},"dadosProfissionais":{"email":"","endereco":{"bairro":"","cep":"","complemento":"","logradouro":"","municipio":"","numero":"","pais":"","uf":""},"instituicao":"DESEMPREGADO","isSocio":null,"profissao":"","ramoAtividade":{"codigo":"541","descricao":"MECANICO DE MANUTENCAO DE VEICULOS AUTOMOTORES E MAQUINAS"},"telefone":{"ddd":"","ddi":"","numero":""}},"dadosRegulatorios":{"fatca":null},"naturezaRelacao":{"cambio":false,"contaCorrente":false,"credito":false,"descricaoOutros":"","fianca":false,"investimentos":false,"outros":false},"pep":{"isPep":"N","possuiRelacaoPep":"N","relacoes":[]},"referenciasBancarias":[],"referenciasComerciais":[],"referenciasPessoais":[],"versao":"PF5.0"}'
		//def cliente = '{"acionistas":[{"cargo":"","insricao":12856123880,"nacionalidade":"","nome":"RENATO DOS SANTOS CASTILHO","participacao":100}],"administradores":[{"cargo":"DIRETOR","insricao":12856123880,"nacionalidade":"","nome":"RENATO DOS SANTOS CASTILHO"}],"bancos":[],"beneficiariosFinais":[{"insricao":12856123880,"nacionalidade":"","nome":"RENATO DOS SANTOS CASTILHO"}],"clientes":[],"contatos":[{"cargo":"ANALISTA","email":"RECEBIVEISCRT@BANCOPAULISTA.COM.BR","nome":"MARIENE","ramal":"(11) 3299-2491"}],"dadosCadastrais":{"clienteSistemaFinanceiroDesde":"2018-08-03","dataConstituicao":"2014-03-31","domiciliadoExterior":"N","email":"","endereco":{"bairro":"JARDIM CASTELO","cep":"03728-180","complemento":"","logradouro":"R PIPERALES","municipio":"SAO PAULO","numero":33,"pais":"BRASIL","uf":"SP"},"enderecoCorrespondencia":{"bairro":"JARDIM CASTELO","cep":"03728-180","complemento":"","logradouro":"R PIPERALES","municipio":"SAO PAULO","numero":33,"pais":"BRASIL","uf":"SP"},"extratoPapel":"N","fax":{"ddd":"","ddi":55,"numero":""},"formaContituicao":{"codigo":"","descricao":""},"idPessoa":"","inscricao":19980856000107,"localCorrespondencia":1,"nire":"","nomeFantasia":"DREAMS HOUSE COMERCIO DE MOVEIS EIRELI-ME","ramoAtividade":{"codigo":4754,"descricao":"COMERCIO VAREJISTA DE ARTIGOS DE ILUMINACAO"},"razaoSocial":"DREAMS HOUSE COMERCIO DE MOVEIS EIRELI-ME","telefone":{"ddd":11,"ddi":55,"numero":32992491},"tipoPessoa":"J","website":""},"dadosFinanceiros":{"capital":93700,"faturamentoMedioMensal":30000,"patrimonio":100000},"dadosRegulatorios":{"fatca":"","giin":"","nif":""},"fornecedores":[],"naturezaRelacao":{"cambio":false,"contaCorrente":false,"credito":false,"descricaoOutros":"","fianca":false,"investimentos":false,"outros":false},"participacoes":[],"pep":{"isPep":"","possuiRelacaoPep":"N","relacoes":[]},"procuradores":[],"produtos":[],"seguros":[],"versao":"PJ5.0"}'
		execution.setVariable("cliente", cliente)
		Map<String,Object> appContext = new HashedMap()
		appContext.dataServicesEndpoint="http://172.16.14.43:8080/esbproxy/services"
		bloqueio.execute(execution, appContext)
	}
}
