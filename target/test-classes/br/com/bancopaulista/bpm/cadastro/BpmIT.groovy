
package br.com.bancopaulista.bpm.cadastro;

import groovy.json.JsonSlurper;
import groovy.util.logging.Log4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.io.IOUtils;

import static org.junit.Assert.*

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.h2.jdbcx.JdbcDataSource;

@Log4j
class BpmIT {


	def resources = [ 
		"diagrams/cadastro-bloqueio.bpmn20.xml"
		 ];
	 
	 def resources_ativacao_only = [
		 "mock_ativacao_backend.bpmn",
		 "diagrams/procHomeBrokerAtivacao.bpmn20.xml"
		  ];
 
 
	 ProcessEngine pe;
	 
 
	
	/**
	 * Registra bases de dados  de teste para uso nos scripts
	 */
	@Before
	public void registerDatabases() {
		Context ctx = new InitialContext();
		
		JdbcDataSource ds = new JdbcDataSource();
		ds.setURL("jdbc:h2:mem:test");
		ds.setUser("sa");
		ds.setPassword("sa");
 
		// 
		ctx.bind("jdbc/testDB", ds);
	}
	
	@Before
	public void initProcessEngine() {
		
		ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("activiti.cfg.xml");
		pe = appContext.getBean(ProcessEngine.class);
	}
	@Test
	public void test() {
		
		
		ProcessEngine pe = ProcessEngines.getDefaultProcessEngine();
		DeploymentBuilder b = pe.getRepositoryService().createDeployment();

		b = b.name("cadastro-bloqueio-JUNIT");
		b = addResources(b, resources);

		Deployment newDep = b.deploy();

		log.info("[I41] deploy ok: id=" + newDep.getId());

		// Recupera o processo
		ProcessDefinition pdef = pe.getRepositoryService()
		.createProcessDefinitionQuery()
		.deploymentId(newDep.getId())
		.singleResult();
		log.info("[I46] process def: id=" + pdef.getId() + ", name="
		+ pdef.getName() + ", key=" + pdef.getKey() + ", category="
		+ pdef.getCategory());

		Map<String, String> properties = new HashMap<String, String>();
		
		// Prepara variaveis para iniciar processo
		properties.put("inscricao", "62032180000140");
		// Dispara instancia do processo
		ProcessInstance pi = pe.getFormService().submitStartFormData(
		pdef.getId(), properties);

		log.info("[I67] Inst�ncia criada: id=" + pi.getId());

		// Executa tarefas de background
		log.info("[I87] Executando jobs...")
		while(true) {
			List pendingJobs = pe.managementService.createJobQuery().executable().list()
			if ( pendingJobs == null || pendingJobs.empty) {
				log.info("[I90] Nenhum job adicionar a executar. Finalizando");
				return;
			}
			
			pendingJobs.each { Job job ->
				log.info("[I94] Executando job id=${job.id}")
				try {
					pe.managementService.executeJob(job.id)
				}
				catch(Throwable t) {
					log.error ("[E102] Erro executando job ${job.id}",t)
				}
			}
		}

	}

	@Test
	public void testPF1() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"70504",
			inscricao:"42838717880",
			nome: "RICARDO CAETANO DOS SANTOS",
			hbHash: "XXX",
			 
		]

		testCommon(properties)
		
	}
	
	@Test
	public void testPF9() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"64894",
			inscricao:"22350637808",
			nome: "LUIS ROBERTO SOUTO VIDIGAL FILHO",
			hbHash: "414D5120514D2E524F434B45542020202A72F75532BE1822"
		]

		testCommon(properties)
		
	}

	@Test
	public void testPF10_Estrangeiro() {
		
		def cliente = loadTestResourceAsString("cliente_pf5.json");
		def dadosBMF = loadTestResourceAsString("cliente_pf5_bmf.json");
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"46532",
			inscricao:"00657026891",
			nome: "DANIEL DAO LIP LEE",
			hbHash: "XXX",
			cliente: cliente,
			dadosBMF: dadosBMF
			 
		]

		testCommon(properties,resources_ativacao_only, "instance", "procHomeBrokerAtivacao" );
		
	}

	

	@Test
	public void testPF10() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"64340",
			inscricao:"22227352892",
			nome: "MAUR�CIO SHOZO INADA",
			hbHash: "414D5120514D2E524F434B45542020205E1F3957D0A16320"
		]

		testCommon(properties)
		
	}

	@Test
	public void testPF11() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"76611",
			inscricao:"3034540833",
			nome: "CLOVIS REINALDO FUGANHOLI",
			hbHash: "JUNIT_PF11"
		]

		testCommon(properties)
		
	}

	@Test
	public void testPF12() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"78093",
			inscricao:"28421672878",
			nome: "ANDRE LUIZ DE SANTANNA MELO",
			hbHash: "JUNIT_PF12"
		]

		testCommon(properties)
		
	}

	@Test
	public void testPF13() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"78660",
			inscricao:"14286048802",
			nome: "KELLI BONADIO DE OLIVEIRA RAMOS",
			hbHash: "JUNIT_PF13"
		]

		testCommon(properties)
		
	}

	
	@Test
	public void testPF5() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"75646",
			inscricao:"83172319172",
			nome: "CLEBER GUSTAVO DA SILVA SIMAS",
			hbHash: "414D5120514D2E524F434B45542020202A72F75532BE1822"
		]

		testCommon(properties)
		
	}
	
	@Test
	public void testPF5a() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"23524",
			inscricao:"09485140860",
			nome: "SILVIA TAMADA TAKEMOTO",
			hbHash: "XXXX"
		]

		testCommon(properties)
		
	}

	@Test
	public void testPF5b() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"75830",
			inscricao:"34496437899",
			nome: "LA�S TAMARUKEMI BUENO SECCHIS MARINHO",
			hbHash: "XXXX"
		]

		testCommon(properties)
		
	}

	@Test
	public void testPF5c() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"75814",
			inscricao:"02335660900",
			nome: "GIOVANI ANTUNES DE LIMA",
			hbHash: "XXXX"
		]

		testCommon(properties)
		
	}
	
	
	@Test
	public void testPF7() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"20012",
			inscricao:"06051791825",
			nome: "DECIO NERI DOMINGOS",
			hbHash: "XXXX"
		]

		testCommon(properties)
		
	}
	
	@Test
	public void testPF8() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"76587",
			inscricao:"03479420834",
			nome: "NELSON RODRIGUES MARTINEZ",
			hbHash: "414D5120514D2E524F434B45542020205BD47B576C195320"
		]

		testCommon(properties)
		
	}


	@Test
	public void testMonitor() {
		
		testPF5();
		
		def properties = [:]

		testCommon(properties, resources, "instance", "monitor_exportacao_sinacor")
		
	}

	@Test
	public void testMany() {
		
		def clientes = loadTestResourceAsString("clientes.csv").split("\n")
		
		clientes.each { cl ->
			
			def fields = cl.split("\t")

			def properties = [
				tipoCliente: 'F',
				codigoInvestidorCBLC:fields[2].trim(),
				inscricao:fields[1].trim(),
				nome: fields[3].trim(),
				hbHash: "XXXX"
			]
	
			testCommon(properties)
	
			
		}
		
		
		
		
	}


	@Test
	public void testPF_RGDuplicado() {
		
		def cliente = loadTestResourceAsString("cliente_pf3.json")
		
		def properties = [
			tipoCliente: 'F',
			inscricao:"10426482719",
			cliente : cliente
		]

		testCommon(properties,resources_ativacao_only,"instance","procAtivacaoCliente")
		
	}

	
	
	@Test
	public void testPF6_Update() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"63278",
			inscricao:"1394546165",
			nome: "JOAO PAULO MANGUSSI COSTA GOMES",
			hbHash: "XXXX"
		]

		testCommon(properties)
		
	}

	@Test
	public void testPF14_Novo() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"80640",
			inscricao:"00343967308",
			nome: "ANA KARLA ALVES DE LIMA",
			hbHash: "XXXX"
		]

		def pi= testCommon(properties)
		
		List<HistoricActivityInstance> steps = pe.historyService
			.createHistoricActivityInstanceQuery()
			.processInstanceId(pi.id)
			.orderByHistoricActivityInstanceStartTime()
			.asc()
			.list();
			
		def stepNames = steps.collect {
			it.activityId	
		}
		
		println stepNames	
		
	}

	@Test
	public void testPF2() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"44030",
			inscricao:"78162254153",
			nome: "GILBERTO DE VITTO",
			hbHash: "414D5120514D2E524F434B45542020202A72F75532BE1822"
		]

		testCommon(properties)
		
	}
	
	@Test
	public void testPF3() {
		
		def properties = [
			tipoCliente: 'F',
			codigoInvestidorCBLC:"74499",
			inscricao:"01742703261",
			nome: "MARIO LEITE DE PINHO JUNIOR",
			hbHash: "414D5120514D2E524F434B45542020202A72F75532BE1822"
		]

		ProcessInstance pi = testCommon(properties)
		
//		def varInstance = pe.historyService
//			.createHistoricVariableInstanceQuery()
//			.variableName("dadosBMF")
//			.processInstanceId(pi.id)
//			.singleResult()
//			
//		def v = varInstance.value
//		v = new JsonSlurper().parseText(v)
//		
//		assertTrue(v.codigoNacionalidade.toString() == "1")
		
	}


	public def testCommonV1(properties) {
		
		
		DeploymentBuilder b = pe.getRepositoryService().createDeployment();

		b = b.name("cadastro-ativacao-sinacor-JUNIT");
		b = addResources(b, resources);

		Deployment newDep = b.deploy();

		log.info("[I41] deploy ok: id=" + newDep.getId());

		// Recupera o processo
		ProcessDefinition pdef = pe.getRepositoryService()
			.createProcessDefinitionQuery()
			.deploymentId(newDep.getId())
			.processDefinitionKey("cadastro_expresso")
			.singleResult();
		
		
		
		// Dispara instancia do processo
		ProcessInstance pi = pe.getFormService().submitStartFormData(
		pdef.getId(), properties);

		log.info("[I67] Inst�ncia criada: id=" + pi.getId());			

		// Executa tarefas de background
		log.info("[I87] Executando jobs...")
		drainJobs(pe);
		
		// Agora estou esperando o ECIN
		def target = pe.runtimeService
			.createExecutionQuery()
			.messageEventSubscriptionName("ecinProcessado")
			.singleResult()
			
		assertNotNull("Temos que ter uma execu��o aguardando ecinProcessado", target)
		
		// cria estrutura para report OK
		def cblcReport = '{"clientes":[]}'
		
		pe.runtimeService.messageEventReceived("ecinProcessado", 
			target.id,
			[cblcReport:cblcReport])

		drainJobs(pe)
		
		// Agora estou esperando o ECIN
		target = pe.runtimeService
			.createExecutionQuery()
			.messageEventSubscriptionName("msgEatdRecebido")
			.singleResult()
			
		assertNotNull("Temos que ter uma execu��o aguardando msgEatdRecebido", target)
		
		
		pe.runtimeService.messageEventReceived("msgEatdRecebido",
			target.id,
			[cblcReport:cblcReport])
		
		
		target = pe.runtimeService
			.createExecutionQuery()
			.messageEventSubscriptionName("msgCadastroSINACORCompleted")
			.singleResult()

		if ( target != null ) {
			pe.runtimeService.messageEventReceived("msgCadastroSINACORCompleted",
				target.id,
				[
					"statusSinacor": "OK"
				])
		}
		

		drainJobs(pe)
		
		def finishedInstance = pe.historyService
			.createHistoricProcessInstanceQuery()
			.processInstanceId(pi.id)
			.singleResult()
			
		//assertTrue("O cadastro tem que finalizar",finishedInstance.endTime!=null);
		
		
		return pi

	}

	public ProcessInstance testCommon(properties, testResources = null, startMode = "form", startProcess = "cadastro_expresso") {
		
		
		DeploymentBuilder b = pe.getRepositoryService().createDeployment();

		b = b.name("cadastro-ativacao-sinacor-JUNIT");
		b = addResources(b, testResources?:resources);

		Deployment newDep = b.deploy();

		log.info("[I41] deploy ok: id=" + newDep.getId());

		// Recupera o processo
		ProcessDefinition pdef = pe.getRepositoryService()
			.createProcessDefinitionQuery()
			.deploymentId(newDep.getId())
			.processDefinitionKey(startProcess)
			.singleResult();
		
		
		
		// Dispara instancia do processo
		
		ProcessInstance pi;
		if ( startMode == "form" ) {		
			pi= pe.getFormService()
			      .submitStartFormData(
					pdef.id, properties);
		}
		else if ( startMode == "instance") {
			
			pi = pe.getRuntimeService()
			       .startProcessInstanceById(pdef.id,properties);			
		}
		else {
			throw new IllegalArgumentException("[I351] startMode inv�lido: ${startMode}")
		}

		log.info("[I67] Inst�ncia criada: id=" + pi.getId());

		// Executa tarefas de background
		log.info("[I87] Executando jobs...")
		drainJobs(pe);
		
		
		def finishedInstance = pe.historyService
			.createHistoricProcessInstanceQuery()
			.processInstanceId(pi.id)
			.singleResult()
			
		//assertTrue("O cadastro tem que finalizar",finishedInstance.endTime!=null);
		
		
		return pi

	}

	/**
	 * Executa jobs em background at� n�o haver mais o que fazer
	 * @param pe
	 * @return
	 */
	def drainJobs(ProcessEngine pe) {
		
		while(true) {
			List pendingJobs = pe.managementService.createJobQuery().executable().list()
			if ( pendingJobs == null || pendingJobs.empty) {
				log.info("[I90] Nenhum job adicionar a executar. Finalizando");
				return;
			}
			
			pendingJobs.each { Job job ->
				log.info("[I94] Executando job id=${job.id}")
				try {
					pe.managementService.executeJob(job.id)
				}
				catch(Throwable t) {
					log.error ("[E102] Erro executando job ${job.id}",t)
				}
			}
		}

	}

	private DeploymentBuilder addResources(DeploymentBuilder b,  resources) {
		
		resources.each { String r ->
			InputStream is = null;

			try {
				if (!r.startsWith("/")) {
					r = "/" + r;
				}

				is = getClass().getResourceAsStream(r);
				if (is != null) {
					b = b.addInputStream(r, is);
				}
				else {
					throw new IllegalArgumentException("[E82] Recurso '" + r
					+ "' n�o encontrado. Verifique classpath");
				}
			}
			finally {
				try {
					if (is != null)	is.close();
				}
				catch (IOException ignored) {
				}
			}
		}
		return b;
	}
	
	protected String loadTestResourceAsString(String r) {
		if (!r.startsWith("/")) {
			r = "/" + r;
		}

		InputStream is = getClass().getResourceAsStream(r);
		if ( is == null ) {
			throw new IllegalArgumentException("Recurso n�o encontrado: ${r}")
		}
		try {
			return IOUtils.toString(is)
		}
		finally {
			is.close()
		}
	}

}
