package cadastro_desativacao

import cadastro_common.ActivitiHelper;
import cadastro_common.AtivacaoHelper;
import cadastro_common.MailHelper;
import cadastro_common.MateraMapper;
import cadastro_common.MateraServices;
import cadastro_common.MateraUtil;
import cadastro_common.XmlHelper;
import groovy.util.logging.Log4j;
import groovy.xml.MarkupBuilder

import java.util.Map;
import org.activiti.engine.delegate.DelegateExecution;
@Log4j
class svcDesativaCliente {
	def inscricao 
	def execute( DelegateExecution execution, Map<String,Object> appContext ) {
		this.inscricao=execution.getVariable('inscricao')
		execution.setVariable("xref",this.inscricao)
		def nomeCliente= execution.getVariable('nomeCliente')
		nomeCliente = nomeCliente == null? null : nomeCliente
		execution.setVariable("nomeCliente", nomeCliente)
		execution.setVariable("idPessoaMatera", null)
		execution.setVariable("clienteAtivo", "S")
		
		def matera = new MateraServices(appContext.dataServicesEndpoint)
		def pessoa = matera.consultarPessoa(MateraUtil.keepDigits(this.inscricao))
		
		if(pessoa != null)
		{
			def clienteMatera = MateraMapper.vbc2update(pessoa);
			log.info("Cliente matera = ${clienteMatera}")
			def idPessoaMatera = clienteMatera['IDPESSOA']
			execution.setVariable("idPessoaMatera",idPessoaMatera)
			if(nomeCliente==null) {
				nomeCliente = clienteMatera['NOME']
				log.info("Nome do cliente est� nulo. Atualizando para nome Matera = ${nomeCliente}")
				execution.setVariable("nomeCliente", nomeCliente)
			}
			execution.setVariable("idPessoaMatera",idPessoaMatera)
			ActivitiHelper.addComment(execution, "Cliente localizado no Matera. ID = ${idPessoaMatera}")
			AtivacaoHelper.saveMsgAtivacao(execution, "MATERA", true, "Cliente localizado no Matera. ID = ${idPessoaMatera}")
			log.info("Cliente ativo atualmente = ${clienteMatera['INDATIVO']}")
			if(clienteMatera['INDATIVO']=="S") {
				if(desativarClienteMatera(appContext, clienteMatera,execution)){
					execution.setVariable("clienteAtivo", "N")
					ActivitiHelper.addComment(execution, "Cliente desativado com sucesso. ID = ${idPessoaMatera}")
					AtivacaoHelper.saveMsgAtivacao(execution, "MATERA", true, "Cliente desativado com sucesso")
				}
				else {
					ActivitiHelper.addComment(execution, "Erro ao desativar cliente no Matera. Desativa��o abortada")
				}
			}
			else {
				log.info("Cliente ${this.inscricao} j� est� desativado no matera.")
				execution.setVariable("clienteAtivo", "N")
				AtivacaoHelper.saveMsgAtivacao(execution, "MATERA", true, "Cliente ${this.inscricao} j� est� desativado no matera.")	
				ActivitiHelper.addComment(execution, "Cliente ${this.inscricao} j� est� desativado no Matera. ID = ${idPessoaMatera}")
				return true
			}
		}else {
			ActivitiHelper.addComment(execution, "Cliente n�o localizado no Matera. Inscri��o= ${this.inscricao}")
			execution.setVariable("nomeCliente", "")
			execution.setVariable("idPessoaMatera", "")
			AtivacaoHelper.saveMsgAtivacao(execution, "MATERA", false, "Cliente n�o localizado no Matera")
		}
	}
	
	
	def desativarClienteMatera(appContext,clienteMatera,execution) {
		try{
			def matera = new MateraServices(appContext.dataServicesEndpoint)
			log.info("Iniciando desativa��o do cliente.")
			clienteMatera['INDATIVO']="N"
			log.info("Dados cliente Matera: ${clienteMatera}")
			matera.atualizarPessoa(clienteMatera)
			log.info("Cliente desativado com sucesso no Matera.")
			return true
		}
		catch(Exception ex) {
			def msg = ex.message
			if ( msg.indexOf("BC-") >= 0 || msg.indexOf("SD-") > 0 ) {
				// Erro de consist�ncia. Tento extrair a mensagem da exce��o para ter
				// um resultado decente para mostrar
				String[] lines = msg.split("\n");
				for( String l : lines ) {
					def idx = Math.max(l.indexOf("BC-"),l.indexOf("SD-"))
					if ( idx >= 0 ) {
						AtivacaoHelper.saveMsgAtivacao(execution,"MATERA",false,l.substring(idx))
					}
				}
				return false
			}
			else {
				// Erro sist�mico. Relan�a como runtime para tentar novamente mais tarde
				log.error("[E217] Erro atualizando cliente  no MATERA: ${ex.message}",ex)
				throw new RuntimeException("[E233] Erro acessando MATERA: ${ex.message}")
			}
		}
		
	}
}
